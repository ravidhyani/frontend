import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { catchError, finalize, tap } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) { }



  get httpOptions() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
  }

  postData(url: string, data) {
    return this._http.post(url, JSON.stringify(data), this.httpOptions).pipe(catchError((err: any) => {
      return throwError(err);
    }));
  }
}
