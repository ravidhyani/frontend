import {
  Component,
  OnInit,
  Self,
  Optional,
  Input,
  Output,
  EventEmitter,
  SimpleChanges,
  ViewChild,
  OnChanges
} from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";
import { MatInput } from "@angular/material";

@Component({
  selector: "app-input",
  templateUrl: "./input.component.html",
  styleUrls: ["./input.component.scss"]
})
export class InputComponent implements ControlValueAccessor, OnInit, OnChanges {
  constructor(
    @Self()
    @Optional()
    public ngControl: NgControl
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  @Input()
  placeholder: string;
  @Input()
  inputStyle: any;
  @Input()
  value: string;
  @Input()
  minLength: number;
  @Input()
  maxLength: number;
  @Input()
  disabled: boolean;
  @Input()
  error: string;
  @Input()
  type: "input" | "number" | "percent" | "textarea" = "input";
  @Input()
  decimal = 0;
  @Input()
  range: string;
  @Output()
  change: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild(MatInput)
  input: MatInput;

  onChange = (value: any) => {
    this.change.emit(value);
    this.onPropagateChange(value);
  };
  onTouched = () => {};
  onPropagateChange = (_) => {};

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.error && this.input) {
      this.input.errorState = !!this.error;
    }
  }

  writeValue(value: any) {
    this.value = value;
    this.onChange(this.value);
    if (this.ngControl && this.ngControl.control) {
      this.ngControl.control.markAsPristine();
    }
  }

  registerOnChange(fn: any) {
    this.onPropagateChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }
}
