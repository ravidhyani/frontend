import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators, ValidationErrors } from '@angular/forms';
import { AppModel } from '../app.model';
import { HttpService } from 'src/service/http.service';


@Component({
    selector: 'add-review',
    templateUrl: './add-review.component.html',
    styleUrls: ['./add-review.component.scss']
})
export class AddReviewComponent {
    title = 'Comment Rating';

    form: FormGroup = this.formBuilder.group(new AppModel.Review());
    response = ""
    constructor(private formBuilder: FormBuilder, private _httpService: HttpService) {

    }

    @Input() fieldStyle: Function = () => { };

    get review(): AppModel.Review {
        return this.form.getRawValue() as AppModel.Review;
    }

    getFormValidations(): Map<string, ValidatorFn | ValidatorFn[] | null> {
        const formValidationMap = new Map<string, ValidatorFn | ValidatorFn[] | null>();
        formValidationMap.set("reviewComments", [Validators.required]);
        formValidationMap.set("reviewRatting", [Validators.required]);
        formValidationMap.set("reviewUserId", [Validators.required]);
        formValidationMap.set("reviewContentId", [Validators.required]);
        return formValidationMap;
    }

    submit() {
        console.log("submitting Api");
        this.validate();
    }

    validate() {
        const formValidationMap = this.getFormValidations();
        const formControlNames = Array.from(formValidationMap.keys());

        formControlNames.map(name => {
            const control = this.form.get(name);
            control.setValidators(formValidationMap.get(name));
            control.markAsTouched();
            control.updateValueAndValidity({ emitEvent: false });

        });

        if (this.form.valid) {
            this.addReviewRating();
        }
        else {
            this.getFormValidationErrors();
        }


    }

    addReviewRating() {
        const _this = this;

        let params = Object.entries({
            ...this.review,

        })
            .map(([key, value]) => `${key}=${value}`)
            .join("&");

        let url = 'http://ratting-mm.193b.starter-ca-central-1.openshiftapps.com/review-ratting/add' + `?${params}`;

        console.log("URL : " + url);
        this._httpService.postData(url, {}).subscribe((data: any) => {
            console.log("data");
            _this.response = JSON.stringify(data);
        });
    }

    getFormValidationErrors() {
        Object.keys(this.form.controls).forEach(field => {
            const control = this.form.get(field);
            control.markAsTouched({ onlySelf: true });
        });
    }


    errorMessage = {
        default: {
            required: "Invalid Entry.",
            MinRequired: "Invalid Entry."

        }
    };

    getErrorMessage(id: string) {
        const control = this.form.get(id);
        const errorObject = this.errorMessage[id] || {};
        const errorMessage =
            errorObject[
            Object.keys(errorObject).find(
                key => control.errors && control.errors[key]
            )
            ] ||
            this.errorMessage.default[
            Object.keys(this.errorMessage.default).find(
                key => control.errors && control.errors[key]
            )
            ] ||
            "";

        if (errorMessage instanceof Function) {
            return errorMessage(control.value);
        }
        return errorMessage;
    }

}
