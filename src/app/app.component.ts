import { Component } from '@angular/core';
import { HttpService } from "../service/http.service"
import { Observable, Observer } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Comment Rating';

  constructor(private _httpService: HttpService) {

  }

  apiList = [
    {
      name: "Add Review"

    },
    {
      name: "Get Review"

    },
    {
      name: "Find All Reviews"

    }

  ]


  onSelect(api)
  {

  }


  addReviewRating() {
    console.log("Add Rating reviews");
    this._httpService.postData("http://ratting-mm.193b.starter-ca-central-1.openshiftapps.com/review-ratting/add?reviewComments=test&reviewRatting=5&reviewUserId=3&reviewContentId=1", "").subscribe((data: any) => {
      console.log("data");
    });
  }
}
